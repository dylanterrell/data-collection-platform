import React, { Component } from 'react';
import './App.css';
import { Route, Switch } from 'react-router-dom';
import LoginScreen from './components/LoginScreen.js';
import Dashboard from './components/Dashboard.js';
import DownloadResources from './components/DownloadResources.js';
import Deployments from './components/Deployments.js';
import Form from './components/Form.js';
<<<<<<< HEAD
var macadd = require('macaddress');
=======
import DownloadExperiment from './components/DownloadExperiment.js';
import RunExperiment from './components/RunExperiment.js';

// var macadd = window.require('macaddress');
>>>>>>> origin/master
// var getmac = window.require('getmac');

class App extends Component {
  constructor() {
    super();
    this.state = {
      stationID: '',
      studyID: '',
      accessToken: '',
      tokenID: '',
      scriptURL: '',
      resourceURL: ''
    };
  }

  onChangeStudyID(name) {
    this.setState({
      studyID: name
    });
  }

<<<<<<< HEAD
  async componentDidMount() {
    // getmac.getMac(function(err, macAddress){
    //   console.log(macAddress);
    //   return macAddress;
    // });
    // await this.setStationID;
=======
  onChangeScriptURL(name) {
    this.setState({
      scriptURL: name
    });
  }

  onSetResourceURL(name) {
    this.setState({
      resourceURL: name
    });
>>>>>>> origin/master
  }

  onSetStationID(name) {
    this.setState({
      stationID: name
    });
  }

  // async componentDidMount() {
  //   getmac.getMac(function(err, macAddress){
  //     console.log(macAddress);
  //     return macAddress;
  //   });
  //   await this.setStationID;
  // }

  render() {
    return (
        <div>
             <Route exact={true} path="/" render={(props) => <LoginScreen {...props} setStationID={this.onSetStationID.bind(this)} />} />
             <Route path="/dashboard" render={(props) => <Dashboard {...props} stationID={this.state.stationID}
                                                                               changeStudyID={this.onChangeStudyID.bind(this)}
                                                                              setResourceURL={this.onSetResourceURL.bind(this)} />} />
             <Route path="/deployments" render={(props) => <Deployments {...props} stationID={this.state.stationID}
                                                                                   studyID={this.state.studyID}
                                                                                   changeScriptURL={this.onChangeScriptURL.bind(this)} />} />
             <Route path="/form" render={(props) => <Form {...props} scriptURL={this.state.scriptURL}
                                                                     studyID={this.state.studyID} />} />
             <Route path="/downloadexperiment" render={(props) => <DownloadExperiment {...props} scriptURL={this.state.scriptURL}
                                                                     studyID={this.state.studyID} />} />
             <Route path="/runexperiment" render={(props) => <RunExperiment {...props} scriptURL={this.state.scriptURL}
                                                                     studyID={this.state.studyID} />} />
             <Route path="/downloadresources" render={(props) => <DownloadResources {...props} studyID={this.state.studyID}
                                                                     stationID={this.state.stationID} resourceURL={this.state.resourceURL} />} />
        </div>
      );
  }
}

export default App;
