import React from 'react';
import { Container, Col, Row } from 'react-grid-system';
import config from '../config.js';
import Header from './subcomponents/Header.js';
import axios from 'axios';
const fs = window.require('fs');
const shell = window.require('electron').shell;
const path = window.require('path');

class DownloadExperiment extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        studyID: this.props.studyID,
        scriptURL: this.props.scriptURL,
        accessToken: '',
        tokenID: ''
      }
    }

    goToDownloadExperiment = () => {
      this.props.history.push("/downloadExperiment");
    }

    fsExistsSync(myDir) {
      try {
        fs.accessSync(myDir);
        return true;
      } catch (e) {
        return false;
      }
    }

    runFile = () => {
      const dir = 'C:/Users/jabra365/Desktop/data-collection-platform/Files/1/experiment.osexp';
      shell.openItem(dir);
    }

    goToNext = () => {
      this.props.history.push("/uploaddata");
    }

  render() {
    const path = 'C:/Users/jabra365/Desktop/data-collection-platform/Files/1/experiment.osexp';
    console.log(path);
    return (
      <div>
        <Header />
          <Container>
            <button onClick={this.goToDownloadExperiment} type="button">Go Back</button><br/>
            <h2>Experiment RunTime</h2>
            <button className ="button" disabled={!this.fsExistsSync(path)} onClick={this.runFile} type="button">Run Experiment</button><br/>
            <button onClick={this.goToNext} type="button">Next</button><br/>
          </Container>
      </div>
    );
  }
}

export default DownloadExperiment;
