import React from 'react';
import { Container, Col, Row } from 'react-grid-system';
import config from '../config.js';
import Header from './subcomponents/Header.js';
<<<<<<< HEAD
const fs = require('fs');
=======
const fs = window.require('fs');
var download = require('download-file');
>>>>>>> origin/master

class Form extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        firstname: '',
        lastName: '',
        age: '',
        sex: '',
        studyID: this.props.studyID,
        scriptURL: this.props.scriptURL,
        accessToken: '',
        tokenID: ''
      };
      this.changeFirstName = this.changeFirstName.bind(this);
      this.changeLastName = this.changeLastName.bind(this);
      this.changeAge = this.changeAge.bind(this);
      this.changeSex = this.changeSex.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    // componentDidMount() {
    //   fs.readFile('.\Files\1\experiment.osexp', (err, data) => {
    //     console.log(data);
    //   });
    // }

    changeFirstName(event) {
      this.setState({firstName: event.target.value});
    }

    changeLastName(event) {
      this.setState({lastName: event.target.value});
    }

    changeAge(event) {
      this.setState({age: event.target.value});
    }

    changeSex(event) {
      this.setState({sex: event.target.value});
    }

    handleSubmit = () => {
      var element = document.createElement("a");
      var participant = {"firstName":this.state.firstName,
                        "lastName":this.state.lastName,
                        "age":this.state.age,
                        "sex":this.state.sex};
      var file = new Blob([JSON.stringify(participant)], {type : 'application/json'});
      element.href = URL.createObjectURL(file);
      element.download = "participantInfo.txt";
      element.click();
      this.props.history.push("/downloadexperiment");
    }

    goToDeployments = () => {
        this.props.history.push("/deployments");
    }

    render() {
      const str = '*Save participant information to Study Directory ' + this.state.studyID + '*';
      return (
        <div>
        <Header />
        <Container>
          <button onClick={this.goToDeployments} type="button">Go Back</button>
          <h2>Participant Information</h2>
            <form>
              First name:<br/>
              <input type="text" value={this.state.value} onChange={this.changeFirstName} /><br/>
              Last name:<br/>
              <input type="text" value={this.state.value} onChange={this.changeLastName} /><br/>
              Age:<br/>
              <input type="number" value={this.state.value} onChange={this.changeAge} /><br/>
              Sex:<br/>
              <input type="text" value={this.state.value} onChange={this.changeSex} /><br/><br/>
              <h4> { str } </h4><br/>
              <button onClick={this.handleSubmit}>Submit Information</button><br/>
            </form>
          </Container>
        </div>
      );
    }
  }

  export default Form;
