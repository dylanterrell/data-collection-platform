import React from 'react';
import { Container, Col, Row } from 'react-grid-system';
import { CognitoUserPool, AuthenticationDetails, CognitoUser } from 'amazon-cognito-identity-js';
import config from '../config.js';
import {withRouter} from "react-router-dom";
import Header from './subcomponents/Header.js';
// var getmac = window.require('getmac');

const AWS = require('aws-sdk');
AWS.config = new AWS.Config();
AWS.config.accessKeyId = "AKIAIAFR2T4J4GGLHDGQ";
AWS.config.secretAccessKey = "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";

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      accessToken: '',
      tokenID: '',
      self: this
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  handleSubmit = async event => {
    event.preventDefault();
      const poolData = {
        UserPoolId : config.cognito.USER_POOL_ID,
        ClientId : config.cognito.APP_CLIENT_ID
      }

      const userPool = new CognitoUserPool(poolData);
      const authData = {
          Username : this.state.username,
          Password : this.state.password,
      }
      const authDetails = new AuthenticationDetails(authData);

      const userData = {
        Username: this.state.username,
        Pool: userPool
      }

      const cognitoUser = new CognitoUser(userData);

      const doSignIn = () => {
        this.props.history.push("/dashboard");
      }

      cognitoUser.authenticateUser(authDetails, {
        onSuccess: function(session) {
          const cognitoUser = userPool.getCurrentUser();

          if (cognitoUser != null) {
          	cognitoUser.getSession(function(err, result) {
          		if (result) {
          			// Add the User's Id Token to the Cognito credentials login map.
          			AWS.config.credentials = new AWS.CognitoIdentityCredentials({
          				IdentityPoolId: 'us-east-2:a75b0ee5-4aaa-493a-a74e-82871ce262a3',
          				Logins: {
          					'cognito-idp.us-east-2.amazonaws.com/us-east-2_rzzJdcQUX': result.getIdToken().getJwtToken()
          				}
          			});
                AWS.config.credentials.refresh(function () {
                  console.log('USER-SERVICE:LOGIN:AWS.config.credentials.identityId -> ' + AWS.config.credentials.identityId);
                });
          		}
          	});
          }
          doSignIn();
        },
        onFailure: function(err) {
           alert("Incorrect username or password");
       },
       newPasswordRequired: function(userAttributes, requiredAttributes) {

            userAttributes.name = authDetails.username;

            cognitoUser.completeNewPasswordChallenge(authDetails.password, userAttributes, this);
        }
      });

    this.setState({
      username: '',
      password: '',
    });
  }

<<<<<<< HEAD
=======
  doSignIn = () => {
  // this.setState({
  //     accessToken: session.getAccessToken().getJwtToken(),
  //     tokenID: session.idToken.jwtToken
  // })
  // console.log(this.state);
    this.props.history.push("/dashboard");
  }

  async componentDidMount() {
    this.props.setStationID(config.station.STATIONID)
  }


>>>>>>> origin/master
  render() {
    return (
      <div>
      <Header />
        <Container>
          <div className="login-form">
          <h2>Login to this station</h2>
          <form onSubmit={this.handleSubmit}>
            Username: <input type="text" name="username" placeholder="Username" onChange={this.handleChange} value={this.state.username} className="login-un"/>
            <br/>
            Password: <input type="password" name="password" placeholder="Password" onChange={this.handleChange} value={this.state.password} className="login-pw"/>
            <br/>
            <input type="submit" value="Enter" className="login-submit"/>
          </form>
          </div>
        </Container>
      </div>
    );
  }
}

export default LoginScreen;
