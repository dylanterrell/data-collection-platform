import React from 'react';
import { Container, Col, Row } from 'react-grid-system';
import config from '../config.js';
import Header from './subcomponents/Header.js';
import axios from 'axios';

const fs = window.require('fs');

class DownloadResources extends React.Component {

  constructor(props){
      super(props)
      this.state = {
          studyID: this.props.studyID,
          stationID: this.props.stationID,
          resourceURL: this.props.resourceURL
      };
  }

  doUnzipResource () {
    
  }


  goToDashboard = () => {
    this.props.history.push("/dashboard");
  }


render() {
  const str = '*Save Resources to Study Directory ' + this.state.studyID + '*';
  return (
    <div>
      <Header />
        <Container>
          <button onClick={this.goToDashboard} type="button">Go Back</button><br/>
          <h2>Resource Download</h2>
          <h4> { str } </h4><br/>
          <form method="get" action={this.state.resourceURL}>
            <button className="button" type="submit">Download Resource </button>
          </form><br/><br/>
          <button className="button" onClick={this.doUnzipResource}>Unzip Resource </button>
        </Container>
    </div>
  );
}
}

export default DownloadResources;
