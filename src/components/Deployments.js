import React from 'react';
import { Container, Col, Row } from 'react-grid-system';
import config from '../config.js';
import Header from './subcomponents/Header.js';
import axios from 'axios';
const fs = require('fs');

class Deployments extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        deployments: [],
        stationID: this.props.stationID,
        studyID: this.props.studyID,
        scriptURL: '',
        accessToken: '',
        tokenID: ''
      }
    }

    goToDashboard = () => {
        this.props.history.push("/dashboard");
    }

    goToForm = () => {
      this.props.changeScriptURL(this.state.scriptURL);
      this.props.history.push("/form");
    }

    async componentDidMount() {
      const URL = 'https://ejqiv1lvy7.execute-api.us-east-1.amazonaws.com/dev/deployments?stationID=' + this.state.stationID + '&studyID=' + this.state.studyID;
      await axios.get(URL)
        .then(response => {
          this.setState({deployments: response.data.Items[0].deployments});
        });
      const URL2 = 'https://ejqiv1lvy7.execute-api.us-east-1.amazonaws.com/dev/studies?stationID=' + this.state.stationID;
      await axios.get(URL2)
          .then(response => {
              Object.keys(response.data.Items).map((key) => {
                  if (response.data.Items[key].studyID == this.state.studyID) {
                    this.setState({
                      scriptURL: response.data.Items[key].scriptURL
                    });
                  }
              });
          })
      }

  render() {
    const display = Object.keys(this.state.deployments).map((key) => {
      if (this.state.deployments[key].pool > this.state.deployments[key].count) {
          return (
              <div className="display-border">
                <h4>Demographic: </h4>
                <h5>{this.state.deployments[key].demographic}</h5>
                <h4>Participants: </h4>
                <h5>{this.state.deployments[key].count}/{this.state.deployments[key].pool}</h5>
                <button disabled={false} onClick={this.goToForm}>Run Experiment</button>
              </div>
          )
        }
      });
    return (
      <div>
        <Header />
          <Container>
            <button onClick={this.goToDashboard} type="button">Go Back</button>
            <h3>Available Deployments: </h3>
            <div> { display } </div>
          </Container>
      </div>
    );
  }
}

export default Deployments;
