import React from 'react';
import { Container, Col, Row } from 'react-grid-system';
import config from '../config.js';
import Header from './subcomponents/Header.js';
import axios from 'axios';

class DownloadExperiment extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        studyID: this.props.studyID,
        scriptURL: this.props.scriptURL,
        accessToken: '',
        tokenID: ''
      }
    }

    goToForm = () => {
      this.props.history.push("/form");
    }

    goToNext = () => {
      this.props.history.push("/runexperiment");
    }


  render() {
    const str = '*Save Experiment Script to Study Directory ' + this.state.studyID + '*';
    return (
      <div>
        <Header />
          <Container>
            <button onClick={this.goToForm} type="button">Go Back</button><br/>
            <h2>Experiment Download</h2>
            <h4> { str } </h4><br/>
            <form method="get" action={this.state.scriptURL}>
              <button className="button" type="submit">Download Experiment </button>
            </form><br/><br/>
            <button onClick={this.goToNext} type="button">Next</button><br/>
          </Container>
      </div>
    );
  }
}

export default DownloadExperiment;
