'use strict';
const AWS = require('aws-sdk');
// AWS.config = new AWS.Config();
// AWS.config.accessKeyId = "AKIAIAFR2T4J4GGLHDGQ";
// AWS.config.secretAccessKey = "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";

AWS.config.update({
  region: 'us-east-2',
  endpoint: 'https://dynamodb.us-east-2.amazonaws.com'
});

const dynamodb = new AWS.DynamoDB.DocumentClient();
const s3 = new AWS.S3();

module.exports.getStudies = (event, context, callback) => {
  const params = {
    TableName: "Studies",
    KeyConditionExpression: "stationID = :sta_id",
    ExpressionAttributeValues: {
        ":sta_id": event.queryStringParameters.stationID
    }
  }
  dynamodb.query(params, (err, data) => {
    if(err) {
      const error = JSON.stringify(err);
      const response = {
        statusCode: 502,
        body: JSON.stringify({ "error": error})
      }
      callback(null, response);
    }
    else {
      const response = {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(data)
      }
      callback(null, response);
    }
  });
};

module.exports.getDeployments = (event, context, callback) => {
  const params = {
    TableName: "Studies",
    KeyConditionExpression: "stationID = :sta_id AND studyID = :stu_id",
    ProjectionExpression: "deployments",
    ExpressionAttributeValues: {
        ":sta_id": event.queryStringParameters.stationID,
        ":stu_id": event.queryStringParameters.studyID
    }
  }
  dynamodb.query(params, (err, data) => {
    if(err) {
      const error = JSON.stringify(err);
      const response = {
        statusCode: 502,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify({ "error": error})
      }
      callback(null, response);
    }
    else {
      const response = {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(data)
      }
      callback(null, response);
    }
  });
}

module.exports.getScriptURL = (event, context, callback) => {
  const params = {
    TableName: "Studies",
    KeyConditionExpression: "stationID = :sta_id AND studyID = :stu_id",
    ProjectionExpression: "scriptURL",
    ExpressionAttributeValues: {
        ":sta_id": event.pathParameters.stationID,
        ":stu_id": event.pathParameters.studyID
    }
  }
  dynamodb.query(params, (err, data) => {
    if(err) {
      const error = JSON.stringify(err);
      const response = {
        statusCode: 502,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify({ "error": error})
      }
      callback(null, response);
    }
    else {
      const response = {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(data)
      }
      callback(null, response);
    }
  });
}

module.exports.writes3 = async (event, context, callback) => {
  let uploadStatus = 0;
  const createPartParams = {
    Bucket: 'data-collection-info-registry',
    Key: event.pathParameters.objectName
  }

  let uploadId;
  await s3.createMultipartUpload(params, (err, data) => {
     if (err) uploadStatus = 1; // an error occurred
     else {
       uploadId = data.UploadId
     }
  });

 const uploadParams1 = {
  Body: event.pathParameters.objectBody1
  Bucket: 'data-collection-info-registry',
  Key: event.pathParameters.objectName,
  PartNumber: 1,
  UploadId: uploadId
 }
 let eTag1;
 await s3.uploadPart(uploadPartParams, (err, data) => {
   if (err) uploadStatus = 1; // an error occurred
   else {
     eTag1 = data.ETag
   }
 });

 const uploadParams2 = {
  Body: event.pathParameters.objectBody2
  Bucket: 'data-collection-info-registry',
  Key: event.pathParameters.objectName,
  PartNumber: 2,
  UploadId: uploadId
 }
 let eTag2;
 await s3.uploadPart(uploadPartParams, (err, data) => {
   if (err) uploadStatus = 1; // an error occurred
   else {
     eTag2 = data.ETag
   }
 });

 const completePartParams = {
  Bucket: "examplebucket",
  Key: "bigobject",
  MultipartUpload: {
   Parts: [
      {
     ETag: eTag1,
     PartNumber: 1
    },
      {
     ETag: eTag2,
     PartNumber: 2
    }
   ]
  },
  UploadId: uploadId
 };
 if(uploadStatus == 0) {
   await s3.completeMultipartUpload(completePartParams, function(err, data) {
     if (err) uploadStatus = 1; // an error occurred
     else {
       const dbParams = {
         TableName: "Sessions",
         Item: {
           "sessionId": 1,
           "bucket": data.Bucket,
           "ETag": data.ETag,
           "Key": data.Key,
           "sessionURL": data.Location
         }
       }

       dynamodb.put(dbParams, (err, data) => {
         if(err) {
           const error = JSON.stringify(err);
           const response = {
             statusCode: 502,
             headers: {
               'Access-Control-Allow-Origin': '*',
               'Access-Control-Allow-Credentials': true,
             },
             body: JSON.stringify({ "error": error})
           }
           callback(null, response);
         }
         else {
           const response = {
             statusCode: 200,
             headers: {
               'Access-Control-Allow-Origin': '*',
               'Access-Control-Allow-Credentials': true,
             },
             body: "Success!"
           }
           callback(null, response);
         }
       });
     }
   });
 } else {
   const response = {
     statusCode: 502,
     headers: {
       'Access-Control-Allow-Origin': '*',
       'Access-Control-Allow-Credentials': true,
     },
     body: "Upload Status returned with value of 1"
   }
   callback(null, response);
 }
}
