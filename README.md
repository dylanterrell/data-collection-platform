# Data Collection Platform
### This project is a work in progress

## Getting Started - Running the application

### Prerequisites
- Make sure you have Git and Node installed on your local machine...
In a terminal(Mac) or Command-Line(Windows), execute the following commands:
```
# Check for Git
git --version
# Check for Node
node --version
```

### Installing
- To get the project up and running, execute the following commands:
```
# Clone the repository to your local machine
git clone https://dylanterrell@bitbucket.org/dylanterrell/data-collection-platform.git

# Change into the data-collection-platform directory
cd data-collection-platform

# Install dependencies using NPM
npm install

# Run the React Dev Server and run the Electron appplication (localhost:3000 will be used without opening a browser window)
npm run electron-dev

```
**Important Notes:**
- To close the application, press Ctrl+C in the terminal window that is running

If you have any issues, refer to [this article](https://medium.freecodecamp.org/building-an-electron-application-with-create-react-app-97945861647c) for assistance.
